from django import forms
from django.db.models import fields
from .models import Note

class NoteForm(forms.ModelForm):
    class Meta: 
        model = Note
        fields = ['to', 'from_who', 'title', 'message']

    to = forms.CharField(label="To", required=True, widget=forms.TextInput(attrs={"type" : "text"})) 
    from_who = forms.CharField(label="From", required=True, widget=forms.TextInput(attrs={"type" : "text"})) 
    title = forms.CharField(label="Title", required=True, widget=forms.TextInput(attrs={"type" : "text"}))
    message = forms.CharField(label="Message", required=True, widget=forms.TextInput(attrs={"type" : "text"}))
