
import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:convert';

class MainContent extends StatelessWidget {
  const MainContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(100, 40, 80, 40),
              child: Text(
                "SELAMAT ANDA BERHASIL LOGIN",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 25),
              ),
            ),
            Container(
              child: Text(
                "------------------------",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 28),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(50, 20, 60, 20),
              child: Text(
                "Welcome dosen !!!",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 25),
              ),
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Platform Belajar Terintegrasi",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 20),
              ),
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Kelas Online Berkualitas",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 32),
              ),
            ),
            SizedBox(
              height: 35,
            ),
            ElevatedButton.icon(
              onPressed: () {},
              label: Text('Lihat Semua Kelas'),
              icon: Icon(Icons.search),
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(200, 50), primary: Colors.purple),
            ),
            SizedBox(
              height: 35,
            ),
            Image.network(
              'https://aplikasi-pbp.herokuapp.com/static/img/hero_choices/purp.png',
              height: 300,
            ),
          ],
        ),
      ),
    );
  }
}