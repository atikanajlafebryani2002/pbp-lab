from .models import Friend
from django import forms
import datetime

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ["name", "npm", "DOB"]
	   
    error_messages = {
        "required" : "Please Type"
    }

    name = forms.CharField(label="Nama Lengkap", required=True, max_length=30, widget=forms.TextInput(attrs={"type" : "text", "placeholder" : "Nama Kamu"})) 
    npm = forms.CharField(label="NPM", required=True, max_length=10, widget=forms.TextInput(attrs={"type" : "text", "placeholder" : "NPM Kamu"})) 
    DOB = forms.DateField(label="Tanggal Lahir", initial=datetime.date.today())
