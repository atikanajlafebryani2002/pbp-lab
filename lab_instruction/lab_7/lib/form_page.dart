import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormPageState();
  }
}

class FormPageState extends State<FormPage> {
  late String _firstname;
  late String _lastname;
  late String _email;
  late String _password;
  late String _phoneNumber;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildFirstName() {
    return TextFormField(
      cursorColor: Colors.purple,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'First Name', 
        prefixIcon: Icon(Icons.account_circle),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Name is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _firstname = value!;
      },
    );
  }

  Widget _buildLastName() {
    return TextFormField(
      cursorColor: Colors.purple,
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.account_circle),
        labelText: 'Last Name',
        border: OutlineInputBorder(),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Name is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _lastname = value!;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      cursorColor: Colors.purple,
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.mail),
        labelText: 'Email',
        border: OutlineInputBorder(),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Email is Required';
        }

        if (!RegExp(
                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(value)) {
          return 'Please enter a valid email Address';
        }

        return null;
      },
      onSaved: (String? value) {
        _email = value!;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      cursorColor: Colors.purple,
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.lock),
        labelText: 'Password',
        border: OutlineInputBorder(),
      ),
      keyboardType: TextInputType.visiblePassword,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Password is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _password = value!;
      },
    );
  }

  Widget _buildPhoneNumber() {
    return TextFormField(
      cursorColor: Colors.purple,
      decoration: const InputDecoration(
        prefixIcon: Icon(Icons.phone_iphone),
        labelText: 'Phone Number',
        border: OutlineInputBorder(),
      ),
      keyboardType: TextInputType.number,
      validator: (String? value) {
        int? calories = int.tryParse(value!);

        if (calories == null || calories <= 0) {
          return 'Phone number is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _phoneNumber = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: AppBar(
        title: Text("Profile"),
        backgroundColor: Colors.purple),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(24),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildFirstName(),
                SizedBox(height: 30),
                _buildLastName(),
                SizedBox(height: 30),
                _buildEmail(),
                SizedBox(height: 30),
                _buildPassword(),
                SizedBox(height: 30),
                _buildPhoneNumber(),
                SizedBox(height: 30),
                TextButton(
                  child: const Text(
                    'Submit',
                    style: 
                    TextStyle(color: Colors.purple, fontSize: 16),
                  ),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }

                    _formKey.currentState!.save();

                    print(_firstname);
                    print(_lastname);
                    print(_email);
                    print(_password);
                    print(_phoneNumber);

                    //Send to API
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
