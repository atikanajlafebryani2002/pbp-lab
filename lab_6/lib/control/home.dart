import 'package:flutter/material.dart';
import 'package:lab_6/widgets/main_drawer.dart';
import 'package:lab_6/control/body.dart';
import '../widgets/main_drawer.dart';

// class HomeLab6 extends StatelessWidget {
//   const HomeLab6({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Learning App'),
//         backgroundColor: Colors.purple[300],
//       ),
//       drawer: MainDrawer(),
//     );
//   }
// }

class HomeLab6 extends StatelessWidget {
  const HomeLab6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Learning App'),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.reorder
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        child: MainContent(),
      ),
    );
  }
}
