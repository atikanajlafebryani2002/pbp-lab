1. Apakah perbedaan antara JSON dan XML?
                JSON                                  |                               XML
- Notasi objek JavaScript                             | - Bahasa Markup dapat diperpanjang
- Mendukung banyak tipe data kompleks (bagan, charts, | - Hanya mendukung string, angka, array 
  dan tipe data non-primitif lainnya)                 |   Boolean dan objek (objek hanya dapat
                                                      |   berisi tipe primitif)
- Jika dibandingkan dengan XML kurang aman            | - Lebih aman jika dibandingkan dengan JSON 
- Tidak dapat menyisipkan komen                       | - Dapat menyisipkan komen 
- Tidak memerlukan end tag                            | - Memerlukan tad pada awal dan akhir 
- Ukuran dokumen besar dan dengan file besar,         | - Ringkas sehingga mudah dibaca, tidak ada tag 
  struktur tag membuatnya besar dan rumit untuk       |   atau data yang tidak terpakai atau kosong, 
  dibaca                                              |   membuat file terlihat sederhana

2. Apakah perbedaan antara HTML dan XML?
                HTML                                  |                              XML
- Berfokus pada penyajian data                        | - Berfokus pada transfer data
- Didorong oleh format                                | - Didorong oleh konten
- Merupakan case insensitive                          | - Merupakan case sensitive
- Tidak mensupport namespaces                         | - Mensupport namespaces 
- Tidak strict untuk tag penutup                      | - Strict untuk tag penutup
- Tagnya terbatas                                     | - Tagnya dapat dikembangkan
- Tagnya telah ditentukan sebelumnya                  | - Tag tidak ditentukan sebelumnya