from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=99)
    from_who = models.CharField(max_length=99)
    title = models.CharField(max_length=99)
    message = models.CharField(max_length=99)
