import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildButtons(String name, IconData icon, Color color) {
    return OutlinedButton.icon(
      onPressed: () {},
      icon: Icon(
        icon,
      ),
      label: Text(name),
      style: OutlinedButton.styleFrom(
        primary: color,
        minimumSize: Size(250, 50),
      ),
    );
  }

  Widget buildListTile(String title, IconData icon) {
    return ListTile(
      leading: Icon(
        icon,
        size: 20,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(15),
            color: Colors.purple,
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Hello dosen !',
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 24,
                      color: Colors.white),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          buildButtons("Profile Dosen", Icons.face, Colors.purple[200]!),
          buildButtons("Logout", Icons.logout, Colors.red[600]!),
          buildButtons("All Courses", Icons.list, Colors.purple[900]!),
          buildButtons("Add Courses", Icons.add_circle, Colors.cyan[800]!),
          buildButtons("My Courses", Icons.book, Colors.cyan[800]!),
          buildButtons("My Quiz", Icons.description, Colors.cyan[800]!),
          SizedBox(
            height: 15,
          ),
          buildListTile('Help', Icons.help_center),
          buildListTile('Contacts', Icons.phone),
          buildListTile('FAQ', Icons.question_answer),
          
        ],
      ),
    );
  }
}